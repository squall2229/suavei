import React from 'react';

require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch';

import Header from './components/header/header';
import Main from './components/main/main';
import ThreatTrends from './components/threat-trends/threat-trends';
import Footer from './components/footer/footer';

class App extends React.Component  {
  render() {
    return (
      <div className="index dsds">
        <Header />
        
        <div className="wrapper container">
            <div className="wrapper-content row no-wrap">
              <Main />

              <ThreatTrends />

            </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;

