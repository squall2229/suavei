import axios from 'axios';

import {
  GET_CUSTOMERS_REQUEST,
  GET_CUSTOMERS_SUCCESS,
  GET_CUSTOMERS_FAIL
} from '../constants';

const getCustomersApi = () => {
  return axios.get(`http://suavei.gtwenty.com/scanresults/1`)
    .then(response => response.data)
}

export const getCustomers = () => {
  return async (dispatch) => {

    dispatch({type: GET_CUSTOMERS_REQUEST, fetching: false})

    try {
      const payload = await getCustomersApi();

      dispatch({
        type: GET_CUSTOMERS_SUCCESS,
        payload,
        fetching: true
      })
    } catch(err) {
      dispatch({
        type: GET_CUSTOMERS_FAIL,
        payload: err,
        error: true
      })
    }
  }
}