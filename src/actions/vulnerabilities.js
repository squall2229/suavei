import {
  GET_VULNERABILITIES_REQUEST,
  GET_VULNERABILITIES_SUCCESS,
  GET_VULNERABILITIES_FAIL
} from '../constants';

export const getVULNERABILITIES = (payload) => {
  return dispatch => {

    dispatch({type: GET_VULNERABILITIES_REQUEST, fetching: false})

    try {
      dispatch({
        type: GET_VULNERABILITIES_SUCCESS,
        payload,
        fetching: true
      })
    } catch(err) {
      dispatch({
        type: GET_VULNERABILITIES_FAIL,
        payload: err,
        error: true
      })
    }
  }
}