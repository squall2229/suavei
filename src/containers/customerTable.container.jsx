import React from 'react';
import {connect} from 'react-redux';
import {getCustomers} from '../actions/customers';

import CustomerTable from '../components/main/scan-results/customer-table/customer-table';

const mapStateToProps = (state) => {
  return {
    customers: state.customers
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCustomers: () => dispatch(getCustomers())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerTable);