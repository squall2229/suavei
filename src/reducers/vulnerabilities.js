import {
  GET_VULNERABILITIES_SUCCESS,
  GET_VULNERABILITIES_REQUEST,
  GET_VULNERABILITIES_FAIL
} from '../constants/.';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case GET_VULNERABILITIES_REQUEST:
      return Object.assign({}, state, {payload: action.payload, fetching: action.fetching});

    case GET_VULNERABILITIES_SUCCESS:
      return Object.assign({}, state, {payload: action.payload, fetching: action.fetching});

    default:
      return state;
  }
}