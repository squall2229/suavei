import {
  GET_CUSTOMERS_REQUEST,
  GET_CUSTOMERS_SUCCESS,
  GET_CUSTOMERS_FAIL
} from '../constants/.';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case GET_CUSTOMERS_REQUEST:
      return Object.assign({}, state, {payload: action.payload, fetching: action.fetching});

    case GET_CUSTOMERS_SUCCESS:
      return Object.assign({}, state, {payload: action.payload, fetching: action.fetching});

    default:
      return state;
  }
}