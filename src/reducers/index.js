import { combineReducers } from 'redux';
import vulnerabilities from './vulnerabilities';
import customers from './customers';

export default combineReducers({
  vulnerabilities,
  customers
})

