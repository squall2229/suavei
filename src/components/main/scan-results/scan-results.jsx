import React from 'react';

import SearchCustomer from './search-customer/search-customer';
import CustomerTableContainer from '../../../containers/customerTable.container';

import './scan-results.scss';

class ScanResults extends React.Component {
  constructor() {
    super();

    this.state = {
      user: 'castro'
    }

    this.changeUser = this.changeUser.bind(this);

  }

  changeUser(event) {
    let value = event.target.value;

    this.setState({ user: value })
  }

  render() {
    return (
      <div className="scan-result col-xs-8 border">
        <div className="scan-result-header">
          <SearchCustomer user={this.state.user} changeUser={this.changeUser} />
        </div>
        <div className="customer-container">
          <div className="customer-selected-container row no-margin middle-xs">

            <button 
              className="button-check"
            >Select all
            </button>

            <button
              className="button-check"
            >Expand all
            </button>

            <span>Selected: 0 of 2 </span>

          </div>
          <CustomerTableContainer user={this.state.user} />
        </div>
      </div>
    );
  }
}

export default ScanResults;