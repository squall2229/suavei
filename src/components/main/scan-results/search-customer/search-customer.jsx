import React from 'react';

import './search-customer.scss';

const SearchCustomer = (props) => {
  return (
    <form className="row middle-xs no-margin">
      <select name="customer" onChange={props.changeUser} >
        <option value="castro">Castro LLC</option>
        <option value="roman">New Roman INC</option>
      </select>
      <input 
        type="search" 
        placeholder="Filter by Customer" 
      />
    </form>
  );
}

export default SearchCustomer;