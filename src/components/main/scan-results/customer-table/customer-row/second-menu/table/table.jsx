import React from 'react';

import './table.scss';

const Table = (props) => {
  return (
    <table className={`second-menu-table ${props.show ? 'show' : 'hide'}`}>
      <thead>
        <tr>
          <th>Port</th>
          <th>Qpd</th>
          <th>Severity</th>
          <th>Vulnerability</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>6002/tcp</td>
          <td>99%</td>
          <td>Compromised</td>
          <td>HTTP Brute Logins with Default Credential</td>
          <td>
            <button className="button-action">Quarantine device</button>
            <button className="button-action">Notify customer</button>
            <button className="button-action">Exploit scan details</button>
            <button className="button-action">Ignore</button>
          </td>
        </tr>
        <tr>
          <td>6002/tcp</td>
          <td>99%</td>
          <td>Compromised</td>
          <td>HTTP Brute Logins with Default Credential</td>
          <td>
            <button className="button-action">Quarantine device</button>
            <button className="button-action">Notify customer</button>
            <button className="button-action">Exploit scan details</button>
            <button className="button-action">Ignore</button>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default Table;
