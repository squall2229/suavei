import React from 'react';

import Table from './table/table';

class SecondMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false
    }

    this.showTable = this.showTable.bind(this);
  }

  showTable() {
    this.setState({
      show: !this.state.show
    })
  }

  render() {
    return (
      <li 
        className="second-menu__element" 
        onClick={this.showTable}
      >
        {this.props.name}
        <Table show={this.state.show} />
      </li>
    );
  }
}

export default SecondMenu; 