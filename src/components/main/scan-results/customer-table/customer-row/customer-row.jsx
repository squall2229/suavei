import React  from 'react';

import SecondMenu from './second-menu/second-menu';

class CustomerRow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false
    }

    this.showList = this.showList.bind(this);
  }

  showList(event) {
    event.preventDefault();

    this.setState({
      show: !this.state.show
    })
  }

  render() {
    const arr = [
      'Casdivo LLC', 
      'Creeper Drone', 
      'Enviro Serv LLC', 
      'Jeff Sheehan roofing',
      'New Roman Inc',
      'Ross Ross',
      'Trowbridge associates'
    ]

    return (
      <div className="row between-xs no-margin middle-xs customer-row">
        <div className="col-xs-1">
          <input 
            className="checkbox-table" 
            type="checkbox"
          />
        </div>
        <div className="col-xs-1">W</div>
        <div className="col-xs-4 column-device" onClick={this.showList}>
          <span>Amazon Echo</span>
        </div>
        <div className="col-xs-3">B00X4WHP5E</div>
        <div className="col-xs">
          <button className="button-action">Quarantine device</button>
          <button className="button-action">Notify customer</button>
          <button className="button-action">Exploit scan details</button>
          <button className="button-action">Ignore</button>
        </div>
        <ul className={`second-menu ${this.state.show ? 'show' : 'hide'}`}>
          {
            arr.map((element, i) => 
            <SecondMenu key={i} name={element} />
          )
          }
        </ul>
      </div>
    );
  }
}

export default CustomerRow;