import React from 'react';

import CustomerRow from './customer-row/customer-row';

import './customer-table.scss';

class CustomerTable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let arr = ['1', '2'];

    return (
    <div className={this.props.user === 'castro' ? 'customer-table' : 'hide'}>
      <div className="customer-thead">
        <div className="row between-xs no-margin customer-row">
          <div className="col-xs-1">
            <input
              className="checkbox-table" 
              type="checkbox"
              ref="checkbox"
            />
          </div>
          <div className="col-xs-1">W</div>
          <div className="col-xs-4">Devices</div>
          <div className="col-xs-3">Model Number</div>
          <div className="col-xs">Actions</div>
        </div>
      </div>
      <div className="customer-tbody">
          {
            arr.map((row, i) =>
              <CustomerRow key={i} />
            )
          }
      </div>
    </div>
  );
  }
}

export default CustomerTable;