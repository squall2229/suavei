import React from 'react';

import Vulnerabilities from './vulnerabilities/vulnerabilities';
import TimeChart from './time-chart/time-chart';
import Manufacturer from './manufacturer/manufacturer';
import Region from './region/region';

import './aside.scss';

const Aside = () => {
  return (
    <aside className="aside col-xs no-padding">
      <Vulnerabilities />
      <TimeChart />
      <Manufacturer />
      <Region />
    </aside>
  );
}

export default Aside;