import React from 'react';
import {connect} from 'react-redux';
import Websocket from 'react-websocket';

import {getVULNERABILITIES} from '../../../../actions/vulnerabilities';

import './vulnerabilities.scss';

class Vulnerabilities extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      high: '...',
      medium: '...',
      low: '...',
      warnings: '...',
      active: '...',
      active_perc: '...',
      resolved: '...',
      resolved_perc: '...'
    }
  }

  getData() {
    const socket = new WebSocket("ws://52.33.209.220:8889/getvulnerabilities");

    socket.onopen = () => {
      console.log('Connection established');
      socket.send('test');
    };

    socket.onclose = (event) => {
      if (event.wasClean) {
          alert('The connection is closed cleanly');
      } else {
          alert('Connection failure');
      }
      alert(`Code: ${event.code} reason: ${event.reason}`);
    };

    socket.onmessage = (event) => {
      this.props.getVulnerabilities(JSON.parse(event.data));

      let {
        high,
        medium,
        low,
        warnings,
        active,
        active_perc,
        resolved,
        resolved_perc
      } = this.props.state

      this.setState({
        high,
        medium,
        low,
        warnings,
        active,
        active_perc,
        resolved,
        resolved_perc
      })
    };

    socket.onerror = (error) => {
      alert(`Error ${error.message}`);
    };
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div className="vulnerabilities border">
        <h2 className="">Vulnerabilities</h2>
        <div className="row start-xs middle-xs no-margin">
          <div className="col-xs no-padding">
            <div className="row no-margin">
              <input 
                type="checkbox" 
                id="active" 
              />
              <label htmlFor="active">Active: {this.state.active}({this.state.active_perc}%)</label>
            </div>
            <div className="row no-margin">
              <input 
                type="checkbox" 
                id="resolved" 
              />
              <label htmlFor="resolved">Resolved: {this.state.resolved}({this.state.resolved_perc}%)</label>
            </div>
          </div>
          <ul className="col-xs no-padding">
            <li>Hight: {this.state.high}</li>
            <li>Medium: {this.state.medium}</li>
            <li>Low: {this.state.low}</li>
            <li>Warnings: {this.state.warnings}</li>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state.vulnerabilities.payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getVulnerabilities: (payload) => dispatch(getVULNERABILITIES(payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Vulnerabilities);