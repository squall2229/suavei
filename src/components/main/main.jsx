import React from 'react';

import ScanStatus from './scan-status/scan-status';
import Aside from './aside/aside';
import ScanResults from './scan-results/scan-results';

const Main = () => {
  return (
    <main className="main col-xs-9" >
      <ScanStatus />
      <div className="wrapper-main row no-margin no-wrap">

        <Aside />

        <ScanResults />

      </div>
    </main>
  );
}

export default Main;
