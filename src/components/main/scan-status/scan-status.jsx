import React from 'react';

import './scan-status.scss';

const ScanStatus = () => {
  return (
    <div className="scan-status border">
      View: Scan Status
    </div>
  );
}

export default ScanStatus;