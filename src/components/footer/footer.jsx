import React from 'react';

import './footer.scss';

const Footer = () => {
  return (
    <footer className="footer border">
      <div className="container">View: Footer</div>
    </footer>
  );
}

export default Footer;