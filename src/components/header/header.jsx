import React from 'react';

import './header.scss';

const Header = () => {
  return (
    <header className="header border">
      <div className="container">View: Header</div>
    </header>
  );
}

export default Header;