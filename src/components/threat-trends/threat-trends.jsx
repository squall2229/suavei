import React from 'react';

import './threat-trends.scss';

const TreatTrends = () => {
  return (
    <aside className="threat-trends col-xs border">
      View: Treat Trends
    </aside>
  );
}

export default TreatTrends;