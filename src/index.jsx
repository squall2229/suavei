import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';
import { AppContainer } from 'react-hot-loader';

import MakeRoutes from './routes';

import './style/style.scss';

//start logger off hot module replacement
// window.addEventListener('message', e => {
//   if ('production' !== process.env.NODE_ENV) {
//     console.clear();
//   }
// });
// end

if (module.hot) {
  module.hot.accept('./routes', () => {
    const MakeRoutes = require('./routes').default;
    render(MakeRoutes);
  });
}

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <Router>
          <Component />
        </Router>
      </Provider>
    </AppContainer>,
    document.getElementById('root')
  )
}

render(MakeRoutes);

