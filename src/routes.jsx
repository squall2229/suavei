import React from 'react';
import {
  Route,
  Switch,
  Redirect,
  withRouter
} from 'react-router-dom';
import {connect} from 'react-redux';
import App from './App';

const makeRoutes = () => {
  return (
    <Route path="/" component={App} />
  );
}

export default withRouter(makeRoutes);
