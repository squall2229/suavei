const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const devFlagPlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'false'))
});

require('babel-polyfill');

module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: [
    'babel-polyfill', 
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './index.jsx'
  ],

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './dist/')
  },

  devtool: 'source-map',

  resolve: {
    extensions: ['.js', '.jsx']
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: {presets: ['es2015', 'react']}
        }],
      },
      {
        test: /\.scss$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader'],
        }))
      },
      {
        test: /\.(png|svg|jpeg)$/,
        loader: 'file-loader',
        options: {
          publicPath: './',
          name: 'img/[name].[ext]'
        }
      },
      {
        test: /\.(ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          publicPath: './',
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },

  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    port: 3000,
    stats: 'minimal',
    hot: true,
    // host: '192.168.1.57',
    publicPath: '/'
  },

  plugins: [
    new ExtractTextPlugin({
      filename: 'style.css',
      allChunks: true,
      disable: false
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    devFlagPlugin
  ]
};
